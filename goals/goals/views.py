#-*- coding: utf-8 -*-
from django.shortcuts import render_to_response
from django.template import RequestContext
from forms import InterestForm, TutorForm


def home(request, second_create=None):
    if request.method == 'POST':
        if int(request.POST.get('form_id',1)) == 1:
            form = InterestForm(request.POST)
            form2 = TutorForm()
            if form.is_valid():
                form.save()
                return render_to_response('base.html', {'create': True, 'form2': form2,}, context_instance=RequestContext(request))
            else:
                return render_to_response('base.html', {'form': form, 'create': False, 'form2': form2}, context_instance=RequestContext(request))
        else:
            form = InterestForm()
            form2 = TutorForm(request.POST)
            if form2.is_valid():
                form2.save()
                return render_to_response('base.html', {'create': True, 'form': form}, context_instance=RequestContext(request))
            else:
                return render_to_response('base.html', {'form': form, 'create': False, 'form2': form2}, context_instance=RequestContext(request))
    else:
        form = InterestForm()
        form2 = TutorForm()
        return render_to_response('base.html', {'form': form, 'create': False, 'form2': form2}, context_instance=RequestContext(request))
