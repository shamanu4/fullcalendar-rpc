#-*- coding: utf-8 -*-
from django.db import models


class Interest(models.Model):
	name = models.CharField(u"Name", max_length=254)
	email = models.EmailField(u"Email Address:", max_length=254)
	subject = models.CharField(u"Subject you will tutor:", max_length=254)
	university = models.CharField(u"University Attended:", max_length=254)
	text = models.TextField(u"What you need from me to be the world’s best tutor in your subject:", blank = True, null = True)

	def __unicode__(self):
		return u"name: %s, e-mail: %s, subject: %s" % (self.name, self.email, self.subject)

class Tutor(models.Model):
	name = models.CharField(u"Name", max_length=254)
	email = models.EmailField(u"Email Address:", max_length=254)
	subject = models.CharField(u"Subject:", max_length=254)
	location = models.CharField(u"Location:", max_length=254)
	text = models.TextField(u"What would make your tutor the best tutor in the world:", blank = True, null = True)

	def __unicode__(self):
		return u"name: %s, e-mail: %s, subject: %s" % (self.name, self.email, self.subject)