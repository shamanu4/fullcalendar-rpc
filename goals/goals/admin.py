#-*- coding: utf-8 -*-
from django.contrib import admin
from models import Interest, Tutor


class InterestAdmin(admin.ModelAdmin):
    search_fields = ('name', 'email', 'subject', 'university', 'text')
    list_filter=['university']
    list_display = ['name', 'email', 'subject', 'university']
admin.site.register(Interest, InterestAdmin)

class TutorAdmin(admin.ModelAdmin):
    search_fields = ('name', 'email', 'subject', 'location', 'text')
    list_filter=['location']
    list_display = ['name', 'email', 'subject', 'location']
admin.site.register(Tutor, TutorAdmin)