from django.forms import ModelForm
from models import Interest, Tutor


class InterestForm(ModelForm):
    class Meta:
        model = Interest

class TutorForm(ModelForm):
    class Meta:
        model = Tutor