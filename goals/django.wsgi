#!/usr/bin/python
# -*- coding: utf-8 -*-
import sys, os

sys.path.insert(0, '/home/ubuntu/tutor-in.com')
sys.path.insert(0, '/home/ubuntu/tutor-in.com/goals')

os.environ['DJANGO_SETTINGS_MODULE'] = 'goals.settings'

import django.core.handlers.wsgi
application = django.core.handlers.wsgi.WSGIHandler()
