﻿var sitemap = 
{
"rootNodes":[
{
"pageName":"Tutor-In",
"type":"Wireframe",
"url":"Tutor-In.html",
"children":[
{
"pageName":"Location tutors",
"type":"Wireframe",
"url":"Location_tutors.html"},
{
"pageName":"Stage 1 - Tutor details - About",
"type":"Wireframe",
"url":"Stage_1_-_Tutor_details_-_About.html",
"children":[
{
"pageName":"Stage 1 - Tutor details -Schedule",
"type":"Wireframe",
"url":"Stage_1_-_Tutor_details_-Schedule.html"},
{
"pageName":"Stage 1 - Tutor details - Feedback",
"type":"Wireframe",
"url":"Stage_1_-_Tutor_details_-_Feedback.html"},
{
"pageName":"Stage 1 - Tutor details - Contact",
"type":"Wireframe",
"url":"Stage_1_-_Tutor_details_-_Contact.html"}]},
{
"pageName":"Tutor's management - About",
"type":"Wireframe",
"url":"Tutor_s_management_-_About.html",
"children":[
{
"pageName":"Edit About section",
"type":"Wireframe",
"url":"Edit_About_section.html"},
{
"pageName":"Edit Experience section",
"type":"Wireframe",
"url":"Edit_Experience_section.html",
"children":[
{
"pageName":"Add\/Edit",
"type":"Wireframe",
"url":"Add_Edit.html"}]},
{
"pageName":"Edit Education section",
"type":"Wireframe",
"url":"Edit_Education_section.html",
"children":[
{
"pageName":"Add\/Edit",
"type":"Wireframe",
"url":"Add_Edit_1.html"}]},
{
"pageName":"Edit subjects",
"type":"Wireframe",
"url":"Edit_subjects.html"}]},
{
"pageName":"Tutor's management - Schedule",
"type":"Wireframe",
"url":"Tutor_s_management_-_Schedule.html"}]}]};
