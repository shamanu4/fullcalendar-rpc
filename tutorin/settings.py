# Django settings for tutorin project.

import os, sys

def rel(*x):
    return os.path.join(os.path.abspath(os.path.dirname(__file__)), *x)

sys.path.insert(0, rel('..', 'lib'))
sys.path.insert(0, rel('..',))
sys.path.insert(0, rel('.',))
sys.path.insert(0, rel('app',))


DEBUG = True
TEMPLATE_DEBUG = DEBUG

ADMINS = (
    ('Maxim Musayev', 'shamanu4@gmail.com'),
)

MANAGERS = ADMINS

ALLOWED_HOSTS = '*'

AUTHENTICATION_BACKENDS = (
    'app.accounts.auth_backends.CustomUserModelBackend',
    'django.contrib.auth.backends.ModelBackend',
)

CUSTOM_USER_MODEL = 'accounts.User'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': '',                      # Or path to database file if using sqlite3.
        'USER': '',                      # Not used with sqlite3.
        'PASSWORD': '',                  # Not used with sqlite3.
        'HOST': '',                      # Set to empty string for localhost. Not used with sqlite3.
        'PORT': '',                      # Set to empty string for default. Not used with sqlite3.
    }
}

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# In a Windows environment this must be set to your system time zone.
TIME_ZONE = 'Europe/Kiev'
USE_TZ = True

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en-us'


SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale.
USE_L10N = True

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = True

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/home/media/media.lawrence.com/media/"
MEDIA_ROOT = rel('media')

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://media.lawrence.com/media/", "http://example.com/media/"
MEDIA_URL = '/media/'

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/home/media/media.lawrence.com/static/"
STATIC_ROOT = rel('static')

# URL prefix for static files.
# Example: "http://media.lawrence.com/static/"
STATIC_URL = '/static/'

# Additional locations of static files
STATICFILES_DIRS = (
    rel('mockups'),
)

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
#    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

# Make this unique, and don't share it with anybody.
SECRET_KEY = '6lb*35+(4k@*)^h5g%aw&amp;(n@q$v-exjx%&amp;b47=##%-1r3((35k'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
#     'django.template.loaders.eggs.Loader',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    "django.contrib.auth.context_processors.auth",
    "django.core.context_processors.request",
    "django.core.context_processors.i18n",
    'django.contrib.messages.context_processors.messages',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    # Uncomment the next line for simple clickjacking protection:
    # 'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'tutorin.urls'

# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = 'tutorin.wsgi.application'

TEMPLATE_DIRS = (
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    rel('templates'),
)

INSTALLED_APPS = (

    #django and libs
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'south',
    'grappelli.dashboard',
    'grappelli',
    'filebrowser',
    'django.contrib.admin',
    'django.contrib.admindocs',
    'crispy_forms',

    #project apps
    'app.main',
    'app.accounts',
    'app.tutors',
    'app.students',
    'app.bookings',
    'app.zooz',
    'app.util',
)

# grapelli
GRAPPELLI_INDEX_DASHBOARD = 'dashboard.CustomIndexDashboard'
FILEBROWSER_DIRECTORY = 'uploads/'

# # maps
# EASY_MAPS_CENTER = (-41.3, 32)

# crispy forms
CRISPY_FAIL_SILENTLY = not DEBUG

# Zooz
ZOOZ_UNIQUE_ID = 'tutor-in-test-id'
ZOOZ_APP_KEY = '****'
# ZOOZ_RESPONSE_TYPE = 'JSon'
ZOOZ_IS_SANDBOX = True
ZOOZ_SANDBOX_API_URL = 'https://sandbox.zooz.co/mobile/ExtendedServerAPI'
ZOOZ_PRODUCTION_API_URL = 'https://app.ZooZ.com/mobile/ExtendedServerAPI'
ZOOZ_API_URL = ZOOZ_SANDBOX_API_URL if ZOOZ_IS_SANDBOX else ZOOZ_PRODUCTION_API_URL
ZOOZ_SANDBOX_WEB_URL = 'https://sandbox.zooz.co/mobile/SecuredWebServlet'
ZOOZ_PRODUCTION_WEB_URL = 'https://app.zooz.com/mobile/SecuredWebServlet'
ZOOZ_WEB_URL = ZOOZ_SANDBOX_WEB_URL if ZOOZ_IS_SANDBOX else ZOOZ_PRODUCTION_WEB_URL
ZOOZ_API_USERNAME = 'shamanu4@gmail.com'
ZOOZ_API_PASSWORD = '****'

# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}

TEST_RUNNER = 'django_selenium.selenium_runner.SeleniumTestRunner'

try:
    from settings_local import *
except ImportError:
    pass
