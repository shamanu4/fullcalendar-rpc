from django.conf.urls import patterns, include, url
from django.contrib import admin
from filebrowser.sites import site as file_browser_site
from django.conf import settings

from api import router

admin.autodiscover()

urlpatterns = patterns('',
    url(r'^grappelli/', include('grappelli.urls')),
    url(r'^admin/filebrowser/', include(file_browser_site.urls)),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^router/$', router, name='router'),
    url(r'^router/api/$', router.api, name='api'),
    url(r'^accounts/', include('app.accounts.urls', namespace="accounts")),
    url(r'^tutors/', include('app.tutors.urls', namespace="tutors")),
    url(r'^students/', include('app.students.urls', namespace="students")),
    url(r'^bookings/', include('app.bookings.urls', namespace="bookings")),
    url(r'', include('app.main.urls', namespace="main")),
)

if settings.DEBUG:
    urlpatterns += patterns('',
        (r'^%s(?P<path>.*)$' % settings.MEDIA_URL[1:],'django.views.static.serve',{'document_root': settings.MEDIA_ROOT, 'show_indexes': True}),
    )