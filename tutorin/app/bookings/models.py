# -*- encoding: utf-8 -*-

from django.db import models
from django.utils.translation import ugettext as _
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes import generic

from datetime import datetime, timedelta
from util import timezone
from app.tutors.models import SLOT_TYPES

ORDER_EXPIRE_DELAY = 10 * 60                        # 10 minutes
PAYMENT_METHOD_ZOOZ = 1
PAYMENT_METHODS = (
    (PAYMENT_METHOD_ZOOZ, _("Zooz")),
)


class Order(models.Model):
    user = models.ForeignKey("accounts.User", related_name='orders', blank=True, null=True)
    sid = models.CharField(max_length=64, blank=True, null=True)
    payment_method = models.PositiveSmallIntegerField(choices=PAYMENT_METHODS, blank=True, null=True)
    created_at = models.DateTimeField(default=timezone.now)
    completed_at = models.DateTimeField(blank=True, null=True)
    expire_at = models.DateTimeField(db_index=True, blank=True, null=True)
    is_complete = models.BooleanField(db_index=True, default=False)
    is_expired = models.BooleanField(db_index=True, default=False)
    amount = models.DecimalField(max_digits=5, decimal_places=2, default=0)
    # link to transaction
    content_type = models.ForeignKey(ContentType, blank=True, null=True)
    object_id = models.PositiveIntegerField(blank=True, null=True)
    transaction = generic.GenericForeignKey('content_type', 'object_id')

    def __unicode__(self):
        return "%s [%s %s]" % (self.user, self.amount, self.currency)

    def renew_expire(self, *args, **kwargs):
        if 'seconds' in kwargs:
            seconds = kwargs.pop('seconds')
        else:
            seconds = ORDER_EXPIRE_DELAY
        self.expire_at = timezone.now() + timedelta(seconds=ORDER_EXPIRE_DELAY)
        self.save(*args, **kwargs)

    def save(self, *args, **kwargs):
        self.amount = self.price
        if not self.expire_at:
            self.renew_expire(*args, **kwargs)
        else:
            super(Order, self).save(*args, **kwargs)

    def expire(self):
        for booking in self.bookings.all():
            booking.delete()
        self.is_expired = True
        self.save()

    def time_remain(self):
        if self.is_complete or self.is_expired:
            return -1
        now = timezone.now()
        if self.expire_at <= now:
            self.expire()
            return -1
        return (self.expire_at - now).seconds

    @classmethod
    def check_expired(cls):
        expired = cls.objects.filter(is_complete=False, expire_at__lte=timezone.now())
        for order in expired:
            order.expire()

    @classmethod
    def create_new_order(cls, user):
        order = Order.objects.create(user=user)
        return order

    @classmethod
    def create_new_order_anonymous(cls, sid):
        order = Order.objects.create(sid=sid)
        return order

    @property
    def price(self):
        return reduce(lambda price, booking: price + (booking.slot_price or 0), self.bookings.all(), 0.0) + 1.0

    @property
    def currency(self):
        return "USD"

    def get_json(self):
        return {
            'id': self.pk,
            'created_at': self.created_at.strftime("%Y-%m-%d %H:%M:%S"),
            'expire_at': self.expire_at.strftime("%Y-%m-%d %H:%M:%S"),
            'time_remain': self.time_remain(),
            'anonymous': True if not self.user else False,
            'price': self.price,
            'currency': self.currency,
        }


class Booking(models.Model):
    order = models.ForeignKey(Order, related_name="bookings")
    slot = models.ForeignKey("tutors.Slot", related_name="bookings")
    user = models.ForeignKey("accounts.User", related_name="bookings", blank=True, null=True)
    slot_type = models.PositiveSmallIntegerField(choices=SLOT_TYPES, blank=True, null=True)
    slot_price = models.DecimalField(_("slot price"), max_digits=5, decimal_places=2, blank=True, null=True)

    def __unicode__(self):
        return "%s %s [%s]" % (self.user, self.slot, self.get_slot_type_display())

    def save(self, *args, **kwargs):
        super(Booking, self).save(*args, **kwargs)
        self.order.save()