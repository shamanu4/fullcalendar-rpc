# -*- encoding: utf-8 -*-
from django.contrib import admin
from .models import Booking, Order


class BookingAdmin(admin.ModelAdmin):
    pass
admin.site.register(Booking, BookingAdmin)

class OrderAdmin(admin.ModelAdmin):
    pass
admin.site.register(Order, OrderAdmin)

