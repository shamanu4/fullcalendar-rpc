# -*- encoding: utf-8 -*-
from lib.djangorpc import Error, Msg, RpcHttpResponse
from util.forms import upost
from util.decorators import get_json
from lib.djangorpc.decorators import form_handler
from django.template.loader import render_to_string
from django.template import RequestContext
from django.utils.translation import ugettext as _
from datetime import datetime
import time

from tutors.models import Slot, SLOT_TYPE_AWAY, SLOT_TYPE_ONLINE, SLOT_TYPE_HOME
from .models import Order, Booking


def extra_kwargs(request, *args, **kwargs):
    return {
        'request': request,
    }


class BookingApiClass(object):

    def order_create(self, slot_id, slot_type, user, request):
        response = RpcHttpResponse()

        try:
            slot = Slot.objects.get(pk=slot_id, bookings__isnull=True)
        except Slot.DoesNotExist:
            response['success'] = False
            response['msg'] = _("can not make booking")
            return response

        if user.is_authenticated():
            order = user.get_active_order(request.session._session_key) or Order.create_new_order(user)
        else:
            try:
                order = Order.objects.get(sid=request.session._session_key)
            except Order.DoesNotExist:
                order = Order.create_new_order_anonymous(request.session._session_key)

        if user.is_authenticated():
            booking = Booking.objects.create(user=user, order=order, slot=slot)
        else:
            booking = Booking.objects.create(order=order, slot=slot)

        slot_type = int(slot_type)
        if not slot_type in (SLOT_TYPE_HOME, SLOT_TYPE_ONLINE, SLOT_TYPE_AWAY):
            response['success'] = False
            response['msg'] = _("illegal slot type")
            return response
        if slot_type == SLOT_TYPE_ONLINE:
            if not slot.is_online:
                response['success'] = False
                response['msg'] = _("illegal slot type")
                return response
            else:
                booking.slot_type = SLOT_TYPE_ONLINE
                booking.slot_price = slot.price_online
        if slot_type == SLOT_TYPE_HOME:
            if not slot.is_home:
                response['success'] = False
                response['msg'] = _("illegal slot type")
                return response
            else:
                booking.slot_type = SLOT_TYPE_HOME
                booking.slot_price = slot.price_home
        if slot_type == SLOT_TYPE_AWAY:
            if not slot.is_away:
                response['success'] = False
                response['msg'] = _("illegal slot type")
                return response
            else:
                booking.slot_type = SLOT_TYPE_AWAY
                booking.slot_price = slot.price_away
        booking.save()

        response['success'] = True
        response['msg'] = _("order saved")
        response['slot'] = slot.get_json()
        response['order'] = order.get_json()
        return response

    order_create._extra_kwargs = extra_kwargs

    def order_renew(self, order_id, user):
        response = RpcHttpResponse()
        try:
            order = Order.objects.get(pk=order_id)
        except Order.DoesNotExist:
            response['success'] = False
            response['msg'] = _("order not found")
        else:
            if order.is_complete:
                response['success'] = False
                response['msg'] = _("order already closed")
            elif order.is_expired:
                response['success'] = False
                response['msg'] = _("order expired")
            elif not order.user == user:
                response['success'] = False
                response['msg'] = _("access denied")
            else:
                order.renew_expire()
                response['success'] = True
                response['msg'] = _("reserve time prolonged")
                response['order'] = order.get_json()
        return response


