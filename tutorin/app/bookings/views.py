# -*- encoding: utf-8 -*-

from util.decorators import render_to
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.conf import settings
from .models import Booking, Order

@login_required
@render_to("bookings/order.html")
def order(request):
    order = request.user.get_active_order(request.session._session_key)
    if not order:
        return HttpResponseRedirect(reverse("main:index"))
    return {
        'order':order,
        'zooz': {
            'is_sandbox': settings.ZOOZ_IS_SANDBOX,
            'unique_id': settings.ZOOZ_UNIQUE_ID
        }
    }

