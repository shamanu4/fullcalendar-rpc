# -*- encoding: utf-8 -*-

from django.db import models
from django.utils.translation import ugettext as _

from util.slug import unique_slugify


class Student(models.Model):
    user = models.OneToOneField("accounts.User", verbose_name=_('user'), related_name="students_profile")
    first_name = models.CharField(_("first name"), max_length=64)
    last_name = models.CharField(_("last name"), max_length=64)
    slug = models.SlugField(_("slug field"), max_length=128, blank=True, null=True)

    @property
    def fullname(self):
        return "%s %s" % (self.first_name, self.last_name)

    def __unicode__(self):
        return self.fullname

    def save(self, *args, **kwargs):
        unique_slugify(self, "%s %s" % (self.first_name, self.last_name))
        super(Student, self).save(*args, **kwargs)

    @models.permalink
    def get_absolute_url(self):
        return ('student.views.details', [str(self.slug)])
