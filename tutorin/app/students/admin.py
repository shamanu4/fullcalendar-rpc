# -*- encoding: utf-8 -*-
from django.contrib import admin
from .models import Student


class StudentAdmin(admin.ModelAdmin):
    pass
admin.site.register(Student, StudentAdmin)

