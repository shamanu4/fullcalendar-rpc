# -*- encoding: utf-8 -*-
from util.decorators import render_to
from django.shortcuts import get_object_or_404
from .models import Student


@render_to("students/details.html")
def details(request, slug):
    student = get_object_or_404(Student, slug=slug)
    return {'student': student}


@render_to("students/details.html")
def profile(request):
    student = request.user.get_profile()
    return {'student': student}
