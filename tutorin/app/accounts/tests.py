# -*- encoding: utf-8 -*-
__author__ = 'shamanu4.at.gmail.dot.com'

from django.test import TestCase
from app.accounts.models import User, USER_ROLE_TUTOR, USER_ROLE_CUSTOMER, USER_ROLE_STAFF
from app.tutors.models import Tutor
from app.students.models import Student

class UserTestCase(TestCase):

    def setUp(cls):
        User.objects.create(username='staff', role=USER_ROLE_STAFF)
        User.objects.create(username='tutor', role=USER_ROLE_TUTOR)
        User.objects.create(username='customer', role=USER_ROLE_CUSTOMER)

    def test_01_tutor_profile_created(self):
        tutor = User.objects.get(username='tutor')
        self.assertIsInstance(tutor.get_profile(), Tutor)

    def test_02_customer_profile_created(self):
        customer = User.objects.get(username='customer')
        self.assertIsInstance(customer.get_profile(), Student)

    def test_03_staff_profile_created(self):
        staff = User.objects.get(username='staff')
        self.assertIsNone(staff.get_profile())