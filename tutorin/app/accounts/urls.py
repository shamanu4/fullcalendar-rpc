from django.conf.urls import patterns, include, url

urlpatterns = patterns('accounts.views',
    url(r'^profile/$', 'profile', name='profile'),
    url(r'^login/$', 'login', {'template_name': 'accounts/login.html'}, name="login"),
    url(r'^logout/$', 'logout', {'template_name': 'accounts/logout.html'}, name="logout"),
    url(r'^register/$', 'register', {'template_name': 'accounts/register.html'}, name="register"),
)
