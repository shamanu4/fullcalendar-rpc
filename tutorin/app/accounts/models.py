# -*- coding: utf-8 -*-

from django.db import models
from django.db.models import signals, Q
from django.contrib.auth.models import User as DjangoUser, UserManager as DjangoUserManager
from django.utils.translation import ugettext as _
from app.tutors.models import Tutor
from app.students.models import Student
from app.bookings.models import Order

USER_ROLE_STAFF = 2
USER_ROLE_TUTOR = 1
USER_ROLE_CUSTOMER = 0

USER_ROLES = (
    (USER_ROLE_STAFF, _("Staff")),
    (USER_ROLE_TUTOR, _("Tutor")),
    (USER_ROLE_CUSTOMER, _("Customer")),
)


def create_profile(sender, instance,  **kwargs):
    profile, new = sender.objects.get_or_create(user=instance)
    return profile

class User(DjangoUser):
    role = models.PositiveSmallIntegerField(choices=USER_ROLES, default=USER_ROLE_CUSTOMER)

    objects = DjangoUserManager()

    @property
    def profile(self):
        if self.role == USER_ROLE_TUTOR:
            try:
                return self.tutor_profile
            except Tutor.DoesNotExist:
                return create_profile(Tutor, self)
        if self.role == USER_ROLE_CUSTOMER:
            try:
                return self.students_profile
            except Student.DoesNotExist:
                return create_profile(Student, self)
        return None

    @property
    def profile_model(self):
        if self.role == USER_ROLE_TUTOR:
            return Tutor
        if self.role == USER_ROLE_CUSTOMER:
            return Student
        return None

    def get_profile(self):
        return self.profile

    def get_active_order(self, sid):
        Order.check_expired()
        try:
            order = Order.objects.get(Q(is_complete=False, is_expired=False, user=self)|Q(is_complete=False, is_expired=False, sid=sid))
        except Order.DoesNotExist:
            return None
        else:
            if not order.user:
                order.user = self
                order.save()
            return order


signals.post_save.connect(create_profile, sender=User.profile_model)


class TutorRegistration(models.Model):
    name = models.CharField(u"Name", max_length=254)
    email = models.EmailField(u"Email Address:", max_length=254)
    subject = models.CharField(u"Subject you will tutor:", max_length=254)
    university = models.CharField(u"University Attended:", max_length=254)
    text = models.TextField(u"What you need from me to be the world’s best tutor in your subject:", blank = True, null = True)

    def __unicode__(self):
        return u"name: %s, e-mail: %s, subject: %s" % (self.name, self.email, self.subject)

class StudentRegistration(models.Model):
    name = models.CharField(u"Name", max_length=254)
    email = models.EmailField(u"Email Address:", max_length=254)
    subject = models.CharField(u"Subject:", max_length=254)
    location = models.CharField(u"Location:", max_length=254)
    text = models.TextField(u"What would make your tutor the best tutor in the world:", blank = True, null = True)

    def __unicode__(self):
        return u"name: %s, e-mail: %s, subject: %s" % (self.name, self.email, self.subject)