# -*- coding: utf-8 -*-

from django.contrib.auth.admin import UserAdmin as DjangoUserAdmin
from django.contrib.auth.models import User as DjangoUser
from django.contrib.auth.forms import UserChangeForm as DjangoUserChangeForm
from django.contrib import admin
from django.utils.translation import ugettext as _
from app.accounts.models import User, TutorRegistration, StudentRegistration


class UserChangeForm(DjangoUserChangeForm):
    class Meta:
        model = User


class UserAdmin(DjangoUserAdmin):
    form = UserChangeForm
    list_display = ('username', 'last_name', 'first_name',
                    'is_staff', 'is_active', 'role')
    fieldsets = (
        (None, {'fields': ('username', 'password')}),
        (_('Personal info'), {'fields': (
            'first_name', 'last_name', 'email', 'role',
        )}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser', 'user_permissions')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
        (_('Groups'), {'fields': ('groups',)}),
    )


admin.site.unregister(DjangoUser)
admin.site.register(User, UserAdmin)


class TutorRegistrationAdmin(admin.ModelAdmin):
    search_fields = ('name', 'email', 'subject', 'university', 'text')
    list_filter=['university']
    list_display = ['name', 'email', 'subject', 'university']
admin.site.register(TutorRegistration, TutorRegistrationAdmin)


class StudentRegistrationAdmin(admin.ModelAdmin):
    search_fields = ('name', 'email', 'subject', 'location', 'text')
    list_filter=['location']
    list_display = ['name', 'email', 'subject', 'location']
admin.site.register(StudentRegistration, StudentRegistrationAdmin)