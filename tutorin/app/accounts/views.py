# -*- encoding: utf-8 -*-

from django.conf import settings
from django.http import HttpResponseRedirect, Http404
from django.template.response import TemplateResponse
from django.shortcuts import resolve_url
from django.core.urlresolvers import reverse
from django.views.decorators.debug import sensitive_post_parameters
from django.views.decorators.cache import never_cache
from django.views.decorators.csrf import csrf_protect
from django.contrib.auth import REDIRECT_FIELD_NAME, login as auth_login, logout as auth_logout, get_user_model
from django.contrib.sites.models import get_current_site
from django.utils.translation import ugettext as _
from django.utils.http import is_safe_url

from util.decorators import render_to
from .models import USER_ROLE_TUTOR, USER_ROLE_CUSTOMER
from .forms import CrispyAuthenticationForm, StudentRegistrationForm, TutorRegistrationForm


def profile(request):
    if request.user.is_authenticated():
        if request.user.role == USER_ROLE_TUTOR:
            return HttpResponseRedirect(reverse("tutors:profile"))
        if request.user.role == USER_ROLE_CUSTOMER:
            return HttpResponseRedirect(reverse("students:profile"))
    return Http404


@sensitive_post_parameters()
@csrf_protect
@never_cache
def login(request, template_name='accounts/login.html',
          redirect_field_name=REDIRECT_FIELD_NAME,
          authentication_form=CrispyAuthenticationForm,
          current_app=None, extra_context=None):
    """
    Displays the login form and handles the login action.
    """
    redirect_to = request.REQUEST.get(redirect_field_name, '')
    print (redirect_field_name, redirect_to)

    if request.method == "POST":
        form = authentication_form(data=request.POST)
        if form.is_valid():

            # Ensure the user-originating redirection url is safe.
            if not is_safe_url(url=redirect_to, host=request.get_host()):
                redirect_to = resolve_url(settings.LOGIN_REDIRECT_URL)

            # Okay, security check complete. Log the user in.
            auth_login(request, form.get_user())

            if request.session.test_cookie_worked():
                request.session.delete_test_cookie()

            return HttpResponseRedirect(redirect_to=redirect_to)
    else:
        form = authentication_form(request, redirect_to=redirect_to)

    request.session.set_test_cookie()

    current_site = get_current_site(request)

    context = {
        'form': form,
        redirect_field_name: redirect_to,
        'site': current_site,
        'site_name': current_site.name,
    }
    if extra_context is not None:
        context.update(extra_context)
    return TemplateResponse(request, template_name, context,
                            current_app=current_app)


def logout(request, next_page=None,
           template_name='accounts/logout.html',
           redirect_field_name=REDIRECT_FIELD_NAME,
           current_app=None, extra_context=None):
    """
    Logs out the user and displays 'You are logged out' message.
    """
    auth_logout(request)

    if redirect_field_name in request.REQUEST:
        next_page = request.REQUEST[redirect_field_name]
        # Security check -- don't allow redirection to a different host.
        if not is_safe_url(url=next_page, host=request.get_host()):
            next_page = request.path

    if next_page:
        # Redirect to this page until the session has been cleared.
        return HttpResponseRedirect(next_page)

    current_site = get_current_site(request)
    context = {
        'site': current_site,
        'site_name': current_site.name,
        'title': _('Logged out')
    }
    if extra_context is not None:
        context.update(extra_context)
    return TemplateResponse(request, template_name, context,
                            current_app=current_app)


def register(request, next_page=None,
           template_name='accounts/register.html',
           redirect_field_name=REDIRECT_FIELD_NAME,
           current_app=None, extra_context=None):
    if request.method == 'POST':
        if int(request.POST.get('form_id',1)) == 1:
            form = TutorRegistrationForm(request.POST)
            form2 = StudentRegistrationForm()
            if form.is_valid():
                form.save()
                context = {'create': True, 'form2': form2,}
            else:
                context = {'form': form, 'create': False, 'form2': form2}
        else:
            form = TutorRegistrationForm()
            form2 = StudentRegistrationForm(request.POST)
            if form2.is_valid():
                form2.save()
                context = {'create': True, 'form': form}
            else:
                context = {'form': form, 'create': False, 'form2': form2}
    else:
        form = TutorRegistrationForm()
        form2 = StudentRegistrationForm()
        context = {'form': form, 'create': False, 'form2': form2}

    return TemplateResponse(request, template_name, context)


