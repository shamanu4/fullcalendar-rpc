# -*- encoding: utf-8 -*-
__author__ = 'maxim'

from django.contrib.auth.forms import AuthenticationForm
from django.core.urlresolvers import reverse
from django.forms import ModelForm
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Fieldset, Button, Submit, Hidden
from crispy_forms.bootstrap import FormActions, PrependedText

from .models import StudentRegistration, TutorRegistration

class CrispyAuthenticationForm(AuthenticationForm):

    def __init__(self, *args, **kwargs):

        if 'redirect_to' in kwargs:
            redirect_to = kwargs.pop('redirect_to')
        else:
            redirect_to = ''
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.form_action = reverse("accounts:login")
        self.helper.layout = Layout(
            'username',
            'password',
            Hidden('next', redirect_to),
            FormActions(
                Submit('login', 'Login'),
            )
        )

        super(CrispyAuthenticationForm, self).__init__(*args, **kwargs)


class StudentRegistrationForm(ModelForm):
    class Meta:
        model = StudentRegistration


class TutorRegistrationForm(ModelForm):
    class Meta:
        model = TutorRegistration