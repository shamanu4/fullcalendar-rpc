# -*- encoding: utf-8 -*-

from django import template
from datetime import datetime
from util.gmap import Location

register = template.Library()

@register.inclusion_tag('main/blocks/slot_search.html', takes_context=True)
def slot_search(context):
    request = context['request']
    location = request.POST.get('location', "")
    if location:
        loc = Location()
        loc.address = location
        if not loc.latitude and not loc.longitude:
             context["slot_search_location_error"] = True
    context["slot_search_location"] = location
    context["slot_search_subject"] = request.POST.get('subject', "")
    try:
        dt = datetime.strptime(request.POST.get('date', ""),"%d-%m-%Y")
    except:
        context["slot_search_date"] = ""
    else:
        context["slot_search_date"] = dt.strftime("%d-%m-%Y")
    return context

@register.inclusion_tag('main/blocks/active_order_menu_link.html', takes_context=True)
def active_order(context):
    request = context['request']
    order = request.user.get_active_order(request.session._session_key)
    context["active_order"] = order
    context["active_order"] = order
    return context