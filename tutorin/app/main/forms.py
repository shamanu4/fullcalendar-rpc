# -*- encoding: utf-8 -*-
__author__ = 'maxim'

from django.core.urlresolvers import reverse
from django import forms
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Fieldset, Button, Submit
from crispy_forms.bootstrap import FormActions, PrependedText

class CrispySearchForm(forms.Form):

    def __init__(self, *args, **kwargs):

        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.form_action = reverse("main:search")
        self.helper.layout = Layout(
            'username',
            'password',
            FormActions(
                Submit('login', 'Login'),
            )
        )

