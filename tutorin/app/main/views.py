# -*- encoding: utf-8 -*-

from django.db.models import Q
from django.utils import timezone as dj_timezone

from util.decorators import render_to
from util.gmap import Location
from datetime import date, datetime, timedelta

from app.tutors.models import Tutor, Slot
from app.bookings.models import Order
from app.util import timezone

from hashlib import md5

tz = dj_timezone.get_current_timezone()
DEFAULT_RANGE_IN_MILES = 200


@render_to("main/index.html")
def index(request):
    return {
        'categories': "lorem ipsum dolor sit amet consectetur adipiscing elit Donec id justo mauris".split(" "),
        'tutors': ['Shaimaa', 'Fatma', 'Maha', 'Reem', 'Farida', 'Aya', 'Shahd', 'Ashraqat', 'Sahar', 'Fatin', 'Dalal', 'Doha', 'Fajr']
    }

@render_to("main/search.html")
def search(request):
    if request.method == 'POST':
        Order.check_expired()
        location = request.POST.get('location')
        subject = request.POST.get('subject')
        ds = request.POST.get('date')

        filters = {'bookings__isnull': True}

        if ds:
            try:
                dt = datetime.strptime(ds,"%d-%m-%Y").replace(tzinfo=tz)
            except:
                filters.update({"start__gt": timezone.today()})
            else:
                de = dt + timedelta(days=1)
                filters.update({"start__lte": de, "end__gte": dt})
        else:
            filters.update({"start__gt": timezone.today()})

        if subject:
            filters.update({"tutor__subjects__name__exact": subject})

        if location:
            loc = Location()
            loc.address = unicode(location)
            tutors_in_range = Tutor.objects.by_prox(loc, DEFAULT_RANGE_IN_MILES)
            slots = Slot.objects.filter(Q(tutor__in=tutors_in_range)&Q(**filters))
        else:
            slots = Slot.objects.filter(**filters)

        return {
            'slots': slots,
        }

    else:
        return {}
