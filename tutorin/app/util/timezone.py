# -*- encoding: utf-8 -*-
__author__ = 'maxim'

from django.utils import timezone


def now():
    return timezone.now().astimezone(timezone.get_current_timezone())


def today():
    return now().replace(hour=0, minute=0, second=0, microsecond=0)