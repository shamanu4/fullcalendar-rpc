# -*- encoding: utf-8 -*-

from django import forms
from django.forms.models import model_to_dict
from django.contrib.humanize.templatetags.humanize import apnumber
from django.template.defaultfilters import pluralize
# from copy import copy


def upost(post, obj):
    '''Updates request's POST dictionary with values from object, for update purposes'''
    # post = copy(post)
    for k,v in model_to_dict(obj).iteritems():
        if not hasattr(v,'__iter__') and k not in post:
            post[k] = v
    return post


class MultiSelectFormField(forms.MultipleChoiceField):
    """
    http://djangosnippets.org/snippets/1200/
    """
    widget = forms.CheckboxSelectMultiple

    def __init__(self, *args, **kwargs):
        self.max_choices = kwargs.pop('max_choices', 0)
        super(MultiSelectFormField, self).__init__(*args, **kwargs)

    def clean(self, value):
        if not value and self.required:
            raise forms.ValidationError(self.error_messages['required'])
        if value and self.max_choices and len(value) > self.max_choices:
            raise forms.ValidationError('You must select a maximum of %s choice%s.'
                    % (apnumber(self.max_choices), pluralize(self.max_choices)))
        return value