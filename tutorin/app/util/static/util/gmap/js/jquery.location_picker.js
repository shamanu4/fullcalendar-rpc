/**
 * Replaces all class = 'location_picker' inputs by a Google Map. The user
 * can then select location using the map. In case of no JS the text input
 * will be visible for manual input (lat, long).
 *
 * @author Sardar Yumatov (ja {.] doma [at} gmail {.] com)
 * @requires jQuery
 */


$(document).ready(function() {
    //============- Settings for all gmap fields -=================
    //default position for empty fields [lat, long], default Ukraine, Kiev
    var default_position = [50.45097082769713,30.5225146102905];
    //zoom level for empty fields, default contains whole Ukraine
    var empty_state_zoom = 7;
    //zoom level when position is specified, we usually want very close zoom
    var positioned_zoom = 15;



    //===============- DO NOT CHANGE BELLOW THIS LINE -=============
    var geocoder = new google.maps.Geocoder();

    //init gmap for all pickers
    $("input.location_picker").each(function() {
        //create address resolver input
        var html = $('<div class="location_picker_address"><input type="text" /><span class="button">Search</span><div style="clear: both"></div></div>');

        html.insertBefore(this);
        var geoinput = $('input', html);
        geoinput.val($(this).val());
        geoinput.keypress(function(ev) {
            if(ev.keyCode == 13) { //on enter, initiate search, prevent submit
                ev.preventDefault();
                geobutton.click();
            }
        });

        var geobutton = $('span', html);

        //create google map
        var map = document.createElement('div');
        map.className = "location_picker_map";
        this.parentNode.insertBefore(map, this);
        $(this).hide(); //and hide this text field

//        var val = this.value.split(',');
        var input_lat = $("#id_location_latitude");
        var input_lng = $("#id_location_longitude");
        var val = [
            input_lat.val(),
            input_lng.val()
        ];
        var center, zoom;
        if (val.length >= 2) {
            center = new google.maps.LatLng(val[0], val[1]);
            zoom = positioned_zoom;
        } else {
            center = new google.maps.LatLng(default_position[0], default_position[1]);
            zoom = empty_state_zoom;
        }

        //init Google Map
        opts = {
            'zoom': zoom,
            'mapTypeControl': true,
            'scrollwheel': true,
            'center': center,
            'mapTypeId': google.maps.MapTypeId.ROADMAP
        };
        gmap = new google.maps.Map(map, opts);

        //place the marker to move
        var marker = new google.maps.Marker({'position': center, 'map': gmap, 'draggable': true});

        //click on a map or marker is dragged to new position
        var self_input = this;
        function handle_move(ev) {
            //set new value into hidden input, will be send back to server
            point = ev.latLng
            input_lat.val(point.lat());
            input_lng.val(point.lng());

            //resolve new position into address if possible
            html.removeClass('error');
            html.addClass('busy');
            geocoder.geocode({'latLng': point}, //'bounds': gmap.getBounds()
                function(addresses, status) {
                    html.removeClass('busy');
                    if(status == google.maps.GeocoderStatus.OK && addresses.length > 0) {
                        $(self_input).triggerHandler('address', [addresses[0].address_components])
                        geoinput.val(addresses[0].formatted_address);
                        $(self_input).val(geoinput.val());
                    } else {
                        html.addClass('error');
                        geoinput.val('');
                    }
            });
        }
        google.maps.event.addListener(marker, 'dragend', handle_move);
        google.maps.event.addListener(gmap, 'click', function(ev) {
            marker.setPoint(ev.latLng);
            handle_move(ev);
        });

        //resolve address
        geobutton.click(function() {
            if(geoinput.val() == '') return;

            html.removeClass('error');
            html.addClass('busy');
            geocoder.geocode({'address': geoinput.val(), 'bounds': gmap.getBounds()},
                function(addresses, status) {
                    html.removeClass('busy');
                    if(status == google.maps.GeocoderStatus.OK && addresses.length > 0) {
                        var point = addresses[0].geometry.location
                        //resolved to a point, move the map, move the marker
                        gmap.panTo(point);
                        gmap.setZoom(positioned_zoom);
                        marker.setPosition(point);
                        self_input.value = point.lat()+','+point.lng();
                        $(self_input).triggerHandler('address', [addresses[0].address_components])
                        //geoinput.val(results[0].formatted_address);
                    } else {
                        html.addClass('error');
                    }
            });
        });
    }); // end of each
}); //end of ready


function location_picker_get_part(addr, part) {
    for(var i = 0; i < addr.length; i++) {
        if(addr[i].types.indexOf(part) >= 0)
            return addr[i].long_name;
    }
    return null;
}

/** Naive, slow search in address components. */
function location_picker_get_component(addr, comp) {
    if(comp instanceof Array) {
        ret = ''
        for(var i = 0; i < comp.length; i++) {
            var val = location_picker_get_part(addr, comp[i]);
            if(val !== null) ret += (ret? ' ': '') + val;
        }
    } else ret = location_picker_get_part(addr, comp);
    return ret == ''? null: ret;
}

/** Attach address resolver to given fields */
function location_picker_populate(el, map) {
    $(el).bind('address', function(ev, addr_components) {
        for(var i = 0; i < map.length; i++) {
            if(!this.form.elements[map[i][0]]) continue;
            var comp = map[i][1];
            var val = null;
            if(comp instanceof Array) {
                for(var j = 0; j < comp.length; j++) {
                    val = location_picker_get_component(addr_components, comp[j]);
                    if(val !== null) break;
                }
            } else {
                val = location_picker_get_component(addr_components, comp);
            }
            this.form.elements[map[i][0]].value = val || '';
        }
    });
}
