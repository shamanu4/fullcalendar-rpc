from django.core.cache import cache
from django.utils.encoding import smart_str, smart_unicode
from hashlib import md5
from gmapi.maps import Geocoder
geocoder = Geocoder()


class Location(object):

    address = None

    def geocode(self):
        results, status_code = geocoder.geocode({'address': self.address })
        if not status_code == "OK":
            return (0,0)
        else:
            try:
                return results[0]['geometry']['location']['arg']
            except:
                return (0,0)

    @property
    def cached(self):
        key = md5(smart_str(self.address)).hexdigest()
        latlng = cache.get(key)
        if not latlng:
            latlng = self.geocode()
        cache.set(key, latlng, 86400)
        return latlng

    @property
    def latitude(self):
        return self.cached[0]

    @property
    def longitude(self):
        return self.cached[1]

    @property
    def coords_tuple(self):
        return (self.longitude, self.latitude)

