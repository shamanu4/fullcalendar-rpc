import json
from django.conf import settings
from django.forms import widgets
from django.utils.safestring import mark_safe

# Maps Google's address resolver
# 'model field name': 'type'
# 'model field name': ('type', 'or type2', 'or type 3')
# 'model field name': ('type', ('or type2', 'concat type3'))
# where type is address component type. the first component having the type is
# the match.
DEFAULT_ADRESS_MAP = {
    'country': 'country',
    'province': 'administrative_area_level_1',
    'city': 'locality', # ('sublocality', 'locality'),
    'zipcode': 'postal_code',
    'address': (('route', 'street_number'), ),
}

class GMapLocationPickerWidget(widgets.TextInput):
    """ Google maps location picker widget """

    class Media:
        css = {
            'all': (settings.STATIC_URL + 'util/gmap/css/location_picker.css',
            )
        }
        js = ("http://maps.google.com/maps/api/js?sensor=false&language=" + settings.LANGUAGE_CODE[:2],
              settings.STATIC_URL + 'js/jquery-1.7.2.min.js',
              settings.STATIC_URL + 'util/gmap/js/jquery.location_picker.js',
        )

    def __init__(self, populate = None, *args, **kwargs):
        """
            Create new widget.

            The populate is expected to be a mapping of info-field to model's
            field. This will be used client-side to auto-fill address information
            if available.

            The following info-fields are recognised:
                Country             -- the country name
                AdministrativeArea  -- province, state etc.
                DependentLocality   -- city, place etc
                PostalCode          -- (part of) postal code
                Thoroughfare        -- street

            These are field reported by Google's LatLong reverse lookup.
            The client code will try to locate {key}Name or {key}Number as value.

            Note: you can use DEFAULT_ADRESS_MAP if your fields have common names
            like country, city etc. It will ignore non-existing fields.
        """
        self.populate = populate
        super(GMapLocationPickerWidget, self).__init__(*args, **kwargs)

    def render(self, name, value, attrs=None):
        """Render as text input, replacing by gmap will happen clientside"""
        if None == attrs:
            attrs = {}

        # FIXME: hack to keep vTextField class in django admin interface (used if no JavaScript is available)
        attrs['class'] = 'location_picker lang_{0}'.format(settings.LANGUAGE_CODE[:2]) + (' {0}'.format(attrs['class']) if 'class' in attrs else '') + (' {0}'.format(self.attrs['class']) if 'class' in self.attrs else '')

        # render as plain text input
        if isinstance(value, (list, tuple)):
            value = ",".join(value)
        elif value is None:
            value = ''

        out = super(GMapLocationPickerWidget, self).render(name, value, attrs)
        if 'id' in attrs and self.populate:
            pop = json.dumps(self.populate.items())
            out += u'<script type="text/javascript">location_picker_populate("#%s", %s)</script>' % (attrs['id'], pop)
            
        return mark_safe(out)
