import re
from django import forms
from django.contrib.admin.widgets import AdminTextInputWidget

from util.gmap.forms.widgets import GMapLocationPickerWidget


class GMapLocationField(forms.Field):
    """Ensures (lat:string, long:string) value is properly validated."""
    widget = GMapLocationPickerWidget
    lat_long_reg = re.compile(r'^\s*(\d+(?:\.\d+)?)\s*,\s*(\d+(?:\.\d+)?)\s*$')

    def __init__(self, *args, **kwargs):
        """
            Create new form field.
            Argument 'populate' will be sent as-is to the widget.
        """
        populate = kwargs.pop('populate', None)
        # FIXME: widget replacement hack!
        # django admin really likes to put his own widgets instead of the default. override.
        widget = kwargs.pop('widget', None)
        if widget == AdminTextInputWidget or isinstance(widget, AdminTextInputWidget):
            # django admin tries to replace our widget
            self.widget = GMapLocationPickerWidget(populate = populate, attrs = {'class': 'vTextField'}) # keep CSS class in admin interface
        else:
            widget = widget or self.widget
            if widget == GMapLocationPickerWidget or isinstance(widget, GMapLocationPickerWidget):
                self.widget = GMapLocationPickerWidget(populate = populate)

        super(GMapLocationField, self).__init__(*args, **kwargs)


    def clean(self, value):
        """Validates the value."""
        value = super(GMapLocationField, self).clean(value)
        if value == u'': # require check is performed by forms.Field
            return None

        mth = self.lat_long_reg.match(value)
        if not mth:
            raise forms.ValidationError(self.error_messages['invalid'])

        return mth.groups()  # (lat, long) in a tuple
