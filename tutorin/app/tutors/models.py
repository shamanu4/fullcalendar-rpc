# -*- encoding: utf-8 -*-

from django.db import models
from django.utils.translation import ugettext as _
from django.utils import timezone

from util.slug import unique_slugify
from util.gmap.models.fields import GMapLocationField

from app.geo.models import LocationManager

current_tz = timezone.get_current_timezone()

def get_photo_path(instance, filename):
    extension = filename.split('.')[-1]
    return "uploads/tutor/%s.%s" % (instance.slug, extension)


class Tutor(models.Model):
    user = models.OneToOneField("accounts.User", verbose_name=_('user'), related_name="tutor_profile")
    first_name = models.CharField(_("first name"), max_length=64)
    last_name = models.CharField(_("last name"), max_length=64)
    photo = models.ImageField(_("photo"), upload_to=get_photo_path, blank=True, null=True)
    about = models.TextField(_("about"), blank=True, null=True)
    subjects = models.ManyToManyField("Subject", verbose_name=_("subjects"), blank=True)
    location = GMapLocationField(_("location"), blank=True, null=True)
    slug = models.SlugField(_("slug field"), max_length=128, blank=True, null=True)

    # location inner fields
    # latitude = location.fields['latitude']
    # longitude = location.fields['longitude']
    # address = location.fields['address']

    objects = LocationManager()

    @property
    def fullname(self):
        return "%s %s" % (self.first_name, self.last_name)

    def __unicode__(self):
        return self.fullname

    def save(self, *args, **kwargs):
        unique_slugify(self, "%s %s" % (self.first_name, self.last_name))
        super(Tutor, self).save(*args, **kwargs)

    @property
    def coords_tuple(self):
        return (self.location.latitude, self.location.longitude)

    @models.permalink
    def get_absolute_url(self):
        return ('tutors.views.details', [str(self.slug)])


class Experience(models.Model):
    tutor = models.ForeignKey(Tutor, verbose_name=_('tutor'), related_name='experience')
    organization = models.CharField(_('organization'), max_length=64)
    position = models.CharField(_('position'), max_length=64)
    description = models.TextField(_('description'))
    period_start = models.DateField(_('work period start'), blank=True, null=True)
    period_end = models.DateField(_('work period end'), blank=True, null=True)
    current = models.BooleanField(_('currently work here'), default=False)

    def __unicode__(self):
        return "%s - %s. %s: %s" % (self.period_start, self.period_end, self.organization, self.position)

    @property
    def period(self):
        if self.period_start:
            s = self.period_start.strftime('%b %Y')
        else:
            s = None
        if self.current:
            e = _("Now")
        elif self.period_end:
            e = self.period_end.strftime('%b %Y')
        else:
            e = None
        if s and e:
            return "%s - %s" % (s, e)
        if s:
            return "%s %s" % (_("from"), s)
        if e:
            return "%s %s" % (_("to"), e)
        return ""

    def get_json(self):
        return {
            'organization': self.organization,
            'position': self.position,
            'description': self.description,
            'period': self.period,
        }



class Education(models.Model):
    tutor = models.ForeignKey(Tutor, verbose_name=_('tutor'), related_name='education')
    university = models.CharField(_('university'), max_length=64)
    degree = models.CharField(_('degree'), max_length=64)
    period_start = models.DateField(_('study period start'), blank=True, null=True)
    period_end = models.DateField(_('study period end'), blank=True, null=True)
    current = models.BooleanField(_('surrently study here'), default=False)

    def __unicode__(self):
        return "%s - %s. %s: %s" % (self.period_start, self.period_end, self.university, self.degree)

    @property
    def period(self):
        if self.period_start:
            s = self.period_start.strftime('%Y')
        else:
            s = None
        if self.current:
            e = _("Now")
        elif self.period_end:
            e = self.period_end.strftime('%Y')
        else:
            e = None
        if s and e:
            return "%s - %s" % (s, e)
        if s:
            return "%s %s" % (_("from"), s)
        if e:
            return "%s %s" % (_("to"), e)
        return ""

    def get_json(self):
        return {
            'university': self.university,
            'degree': self.degree,
            'period': self.period,
        }



class Subject(models.Model):
    name = models.CharField(_("name"), max_length=64, unique=True)

    def __unicode__(self):
        return self.name

    def get_json(self):
        return {
            'name': self.name
        }


SLOT_TYPE_ONLINE = 0
SLOT_TYPE_HOME = 1
SLOT_TYPE_AWAY = 2

SLOT_TYPES = (
    (SLOT_TYPE_ONLINE, _("Online")),
    (SLOT_TYPE_HOME, _("Home")),
    (SLOT_TYPE_AWAY, _("Away"),)
)


class Slot(models.Model):
    tutor = models.ForeignKey(Tutor, verbose_name=_('tutor'), related_name="slots")
    start = models.DateTimeField(_("start"))
    end = models.DateTimeField(_("end"))
    is_online = models.BooleanField(_("online"), default=False) # we don't use multiple choices field to make ...
    is_home = models.BooleanField(_("at home"), default=False)  # .. search by slot_type fast and easy
    is_away = models.BooleanField(_("away"), default=False)     #
    price_online = models.DecimalField(_("online price"), max_digits=5, decimal_places=2, blank=True, null=True)
    price_home = models.DecimalField(_("at home price"), max_digits=5, decimal_places=2, blank=True, null=True)
    price_away = models.DecimalField(_("away price"), max_digits=5, decimal_places=2, blank=True, null=True)
    title = models.CharField(_("title"), max_length=32, blank=True, default="")

    def __unicode__(self):
        return "%s - %s" % (self.start, self.end)

    # @property
    # def start_timestamp(self):
    #     return time.mktime(self.start.timetuple())
    #
    # @property
    # def end_timestamp(self):
    #     return time.mktime(self.end.timetuple())

    @property
    def hint(self):
        h = []
        if self.is_online:
            h.append('online')
        if self.is_away:
            h.append('away')
        if self.is_home:
            h.append('home')
        return ", ".join(h)

    def get_json(self):

        return {
            'id': self.pk,
            'tutor': self.tutor.fullname,
            'start': current_tz.normalize(self.start).strftime("%Y-%m-%dT%H:%M:%S"),
            'end': current_tz.normalize(self.end).strftime("%Y-%m-%dT%H:%M:%S"),
            'is_online': self.is_online,
            'is_home': self.is_home,
            'is_away': self.is_away,
            'price_online': self.price_online,
            'price_home': self.price_home,
            'price_away': self.price_away,
            'allDay': False,                     # for fullcalendar
            'title': "%s (%s)" % (self.title, self.hint)
        }