from django.conf.urls import patterns, include, url

urlpatterns = patterns('tutors.views',
    url(r'^profile/$', 'profile', name='profile'),
    url(r'^(?P<slug>.+)/$', 'details', name='details'),
)
