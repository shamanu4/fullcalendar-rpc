$(document).ready(function () {

    var form_errors_display = function (form, data) {
        for (key in data.error) {
            if (data.error.hasOwnProperty(key)) {
                var $field = $('input[name="' + key + '"], textarea[name="' + key + '"]', form);
                var error = '<p class="error_list">' + data.error[key] + '</p>';
                if ($field.length) {
                    $field.before(error);
                } else {
                    $('.global-errors', form).prepend(error);
                }
            }
        }
    };

    var form_clear_delete_action = function (form, data) {
        $('input[name=action_delete]', form).remove();
    };


    $(".delete-action").live('click', function () {
        var form = $(this).closest('form');
        form.append('<input type="hidden" name="action_delete" value="1">');
        form.submit();
    });

    window.TutorPage = {
        update_subjects: function () {
            TutorApi.subjects_get(function (data) {
                var sc = $("#subjects-container-js p");
                sc.html('');
                $.each(data, function (i, item) {
                    if (i > 0) {
                        sc.append('<br />');
                    }
                    sc.append(item.name);
                })
            })
        },

        update_about: function () {
            TutorApi.about_get(function (data) {
                $("#about-container-js").html(data);
            })
        },

        update_education: function () {
            TutorApi.education_get(function (data) {
                var ec = $("#education-container-js");
                ec.html('');
                $.each(data, function (i, item) {
                    ec.append("<p><strong>" + item.degree + " at " + item.university + "</strong><br>" +
                        " <small>" + item.period + "</small></p>");
                })
            })
        },

        update_education_form: function () {
            TutorApi.education_form_get(function (data) {
                $("#education-tutor-popup").html(data);
            });
        },

        update_education_change_form: function (edu_id) {
            TutorApi.education_change_form_get(edu_id, function (data) {
                $("#edit-education-tutor-popup").html(data);
                TutorPage.forms.education.ajax_init();
            });
        },

        update_education_add_form: function () {
            TutorApi.education_add_form_get(function (data) {
                $("#edit-education-tutor-popup").html(data);
                TutorPage.forms.education.ajax_init();
            })
        },

        update_experience: function () {
            TutorApi.experience_get(function (data) {
                var ec = $("#experience-container-js");
                ec.html('');
                $.each(data, function (i, item) {
                    ec.append("<p><strong>" + item.position + " at " + item.organization + "</strong><br>" +
                        " <small>" + item.period + "</small></p>");
                })
            })
        },

        update_experience_form: function () {
            TutorApi.experience_form_get(function (data) {
                $("#experience-tutor-popup").html(data);
            });
        },

        update_experience_change_form: function (edu_id) {
            TutorApi.experience_change_form_get(edu_id, function (data) {
                $("#edit-experience-tutor-popup").html(data);
                TutorPage.forms.experience.ajax_init();
            });
        },

        update_experience_add_form: function () {
            TutorApi.experience_add_form_get(function (data) {
                $("#edit-experience-tutor-popup").html(data);
                TutorPage.forms.experience.ajax_init();
            })
        },

//        ajax forms begin
        forms: {
            subjects: {
                ajax_init: function () {
                    $('#rpc-subjects-set-js').ajaxForm({
                        type: 'RPC',
                        api: {
                            submit: TutorApi.subjects_set
                        },
                        success: function (data, rpc_response, $form) {
                            if (data.success) {
                                $("#edit-subject-tutor-popup").modal('hide');
                                TutorPage.update_subjects();
                                form_clear_delete_action($form, data);
                            } else {
                                form_errors_display($form, data);
                            }
                        },
                        beforeSubmit: function (formData, $form, options) {
                            $('p.error_list', $form).remove();
                        }
                    });
                }
            },
            about: {
                ajax_init: function () {
                    $('#rpc-about-set-js').ajaxForm({
                        type: 'RPC',
                        api: {
                            submit: TutorApi.about_set
                        },
                        success: function (data, rpc_response, $form) {
                            if (data.success) {
                                $("#edit-about-tutor-popup").modal('hide');
                                TutorPage.update_about();
                                form_clear_delete_action($form, data);
                            } else {
                                form_errors_display($form, data);
                            }
                        },
                        beforeSubmit: function (formData, $form, options) {
                            $('p.error_list', $form).remove();
                        }
                    });
                }
            },
            education: {
                ajax_init: function () {
                    $('#rpc-education-set-js').ajaxForm({
                        type: 'RPC',
                        api: {
                            submit: TutorApi.education_set
                        },
                        success: function (data, rpc_response, $form) {
                            if (data.success) {
                                $("#edit-education-tutor-popup").modal('hide');
                                TutorPage.update_education_form();
                                TutorPage.update_education();
                                form_clear_delete_action($form, data);
                            } else {
                                form_errors_display($form, data);
                            }
                        },
                        beforeSubmit: function (formData, $form, options) {
                            $('p.error_list', $form).remove();
                        }
                    });
                }
            },
            experience: {
                ajax_init: function () {
                    $('#rpc-experience-set-js').ajaxForm({
                        type: 'RPC',
                        api: {
                            submit: TutorApi.experience_set
                        },
                        success: function (data, rpc_response, $form) {
                            if (data.success) {
                                $("#edit-experience-tutor-popup").modal('hide');
                                TutorPage.update_experience_form();
                                TutorPage.update_experience();
                                form_clear_delete_action($form, data);
                            } else {
                                form_errors_display($form, data);
                            }
                        },
                        beforeSubmit: function (formData, $form, options) {
                            $('p.error_list', $form).remove();
                        }
                    });
                }
            },
            slot: {
                ajax_init: function () {
                    $('#rpc-slots-set-js').ajaxForm({
                        type: 'RPC',
                        api: {
                            submit: TutorApi.slots_set
                        },
                        success: function (data, rpc_response, $form) {
                            if (data.success) {
                                $('#calendar').fullCalendar('refetchEvents');
                                $('#add-event-popup').modal('hide');
                            } else {
                                form_errors_display($form, data);
                                if (TutorPage.forms.slot.rollback && typeof TutorPage.forms.slot.rollback === 'function') {
                                    TutorPage.forms.slot.rollback();
                                }
                            }
                            TutorPage.forms.slot.rollback = null;
                            form_clear_delete_action($form, data);
                        },
                        error: function (data, rpc_response, $form) {
                            if (TutorPage.forms.slot.rollback && typeof TutorPage.forms.slot.rollback === 'function') {
                                TutorPage.forms.slot.rollback();
                            }
                            TutorPage.forms.slot.rollback = null;
                        },
                        beforeSubmit: function (formData, $form, options) {
                            $('p.error_list', $form).remove();
                        }
                    });
                },
                rollback: null
            }
        },
//        ajax forms end

//        calendar begin
        calendar: {
            slots_get: function(start, end, callback) {
                TutorApi.slots_get(start, end, function(data) {
                    callback(data);
                });
            },

            slot_update_form: function (slot, start, end) {
                if (!slot) {
                    // new slot creation. calendar select event
                    $("#event_start").val($.fullCalendar.formatDate(start, "yyyy-MM-dd HH:mm:ss"));
                    $("#event_end").val($.fullCalendar.formatDate(end, "yyyy-MM-dd HH:mm:ss"));
                    $("#event_instance_id").val('');
                } else {
                    // slot update. calendar eventClick event
                    $("#event_start").val($.fullCalendar.formatDate(slot.start, "yyyy-MM-dd HH:mm:ss"));
                    $("#event_end").val($.fullCalendar.formatDate(slot.end, "yyyy-MM-dd HH:mm:ss"));
                    $("#event_online_chk").attr('checked', slot.is_online);
                    $("#event_away_chk").attr('checked', slot.is_away);
                    $("#event_home_chk").attr('checked', slot.is_home);
                    $("#event_instance_id").val(slot.id);
                }
            }
        }
//        calendar end
    };

    TutorPage.update_education_form();
    TutorPage.update_experience_form();
    TutorPage.forms.subjects.ajax_init();
    TutorPage.forms.about.ajax_init();
    TutorPage.forms.slot.ajax_init();
});


