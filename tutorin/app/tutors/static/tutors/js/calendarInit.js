$(document).ready(function () {
    var calendar = $('#calendar').fullCalendar({
        events: function(start, end, callback){
            TutorPage.calendar.slots_get(start, end, callback);
        },

        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'agendaDay,agendaWeek,month'
        },
        allDaySlot: false,
        timeFormat: 'H(:mm)',
        defaultView: 'agendaWeek',
        selectable: true,
        selectHelper: true,
        unselectAuto: true,
        select: function (start, end, allDay) {
            $('#add-event-popup').modal('show');
            TutorPage.calendar.slot_update_form(null, start, end);
        },
        editable: true,
        eventRender: function (event, element, view) {
            var eventEvent = event;
            $(element).append('<i class="bt-close">x</i>');
            $(element).children('.bt-close').on('click', function () {
                calendar.fullCalendar('removeEvents', eventEvent._id);

            });
        },
        eventDrop: function (event, dayDelta, minuteDelta, allDay, revertFunc, jsEvent, ui, view) {
            TutorPage.forms.slot.rollback = revertFunc;
            TutorPage.calendar.slot_update_form(event);
            $('#rpc-slots-set-js').submit();
        },
        eventResize: function (event, dayDelta, minuteDelta, revertFunc, jsEvent, ui, view) {
            TutorPage.forms.slot.rollback = revertFunc;
            TutorPage.calendar.slot_update_form(event);
            $('#rpc-slots-set-js').submit();
        },
        eventClick: function(calEvent, jsEvent, view) {
            $('#add-event-popup').modal('show');
            TutorPage.calendar.slot_update_form(calEvent);
        }
    });
    $(calendar).fullCalendar('today');
});


