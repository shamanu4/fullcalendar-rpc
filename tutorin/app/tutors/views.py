# -*- encoding: utf-8 -*-
from util.decorators import render_to
from django.shortcuts import get_object_or_404
from .models import Tutor


@render_to("tutors/details.html")
def details(request, slug):
    print slug
    tutor = get_object_or_404(Tutor, slug=slug)
    return {'tutor': tutor}


@render_to("tutors/edit.html")
def profile(request):
    tutor = request.user.get_profile()
    return {'tutor': tutor}
