# -*- encoding: utf-8 -*-
from django.contrib import admin
from .models import Tutor, Education, Experience, Subject, Slot
from .forms import TutorForm

class TutorAdmin(admin.ModelAdmin):
    form = TutorForm
admin.site.register(Tutor, TutorAdmin)


class EducationAdmin(admin.ModelAdmin):
    pass
admin.site.register(Education, EducationAdmin)


class ExperienceAdmin(admin.ModelAdmin):
    pass
admin.site.register(Experience, ExperienceAdmin)


class SubjectAdmin(admin.ModelAdmin):
    pass
admin.site.register(Subject, SubjectAdmin)


class SlotAdmin(admin.ModelAdmin):
    pass
admin.site.register(Slot, SlotAdmin)

