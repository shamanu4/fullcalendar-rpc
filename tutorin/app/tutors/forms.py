# -*- encoding: utf-8 -*-
from django import forms
from django.db.models import Q
from django.utils.translation import ugettext as _

from lib.djangorpc import RpcMultiValueDict
from .models import Tutor, Experience, Education, Subject, Slot

from util.gmap.forms.widgets import GMapLocationPickerWidget
from app.accounts.models import User


class TutorForm(forms.ModelForm):
    subjects = forms.CharField(widget=forms.Textarea(), required=False)

    class Meta:
        model = Tutor
        widgets = {
            'location_address': GMapLocationPickerWidget({'class': 'vTextField'})
        }

    def __init__(self, *args, **kwargs):
        instance = kwargs.get('instance')
        if instance:
            initial = {}
            initial['subjects'] = ", ".join(map(lambda x: x.__unicode__(), instance.subjects.all()))
            kwargs['initial'] = initial
            if args:
                if isinstance(args[0], RpcMultiValueDict) and not 'subjects' in args[0]:
                    args[0]['subjects'] = initial['subjects']
        super(TutorForm, self).__init__(*args, **kwargs)

    def clean_subjects(self):
        data = self.cleaned_data['subjects']
        subjects = data.split(',')
        data = []
        for item in subjects:
            item = item.strip().lower()
            if not item:
                continue
            subject, new = Subject.objects.get_or_create(name=item)
            data.append(subject)
        return data

    def save(self, commit=True):
        instance = forms.ModelForm.save(self, False)

        old_save_m2m = self.save_m2m

        def save_m2m():
           old_save_m2m()
           # This is where we actually link the pizza with toppings
           instance.subjects.clear()
           for subject in self.cleaned_data['subjects']:
               instance.subjects.add(subject)

        self.save_m2m = save_m2m

        # Do we need to save all changes now?
        if commit:
            instance.save()
            self.save_m2m()

        return instance


class ExperienceForm(forms.ModelForm):
    class Meta:
        model = Experience


class EducationForm(forms.ModelForm):
    class Meta:
        model = Education


class SubjectForm(forms.ModelForm):
    class Meta:
        model = Subject


class SlotForm(forms.ModelForm):
    class Meta:
        model = Slot

    def clean(self):
        start = self.cleaned_data['start']
        end = self.cleaned_data['end']
        tutor = self.cleaned_data['tutor']
        clashed_slots = Slot.objects.filter(Q(tutor=tutor) & (
                                             Q(start__gt=start) & Q(start__lt=end) |
                                             Q(end__gt=start) & Q(end__lt=end) |
                                             Q(start__lte=start) & Q(end__gte=end)
                                           )
        )
        if self.instance:
            clashed_slots = clashed_slots.filter(~Q(id__exact=self.instance.pk))
        if clashed_slots.count():
            raise forms.ValidationError(_("Clashed slots detected"))
        return self.cleaned_data