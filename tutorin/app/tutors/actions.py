# -*- encoding: utf-8 -*-
from lib.djangorpc import Error, Msg, RpcHttpResponse
from util.forms import upost
from util.decorators import get_json
from lib.djangorpc.decorators import form_handler
from django.template.loader import render_to_string
from django.template import RequestContext
from django.utils.translation import ugettext as _
from datetime import datetime
import time

from .forms import TutorForm, EducationForm, ExperienceForm, SlotForm
from .models import Education, Experience, Slot


def extra_kwargs(request, *args, **kwargs):
    return {
        'request': request,
    }


class TutorApiClass(object):

    @get_json
    def subjects_get(self, user):
        return user.get_profile().subjects.all()

    @form_handler
    def subjects_set(self, rdata, user):
        form = TutorForm(upost(rdata,user.get_profile()), instance=user.get_profile())
        if form.is_valid():
            form.save()
            response = RpcHttpResponse()
            response['success'] = True
            response['msg'] = "subjects saved"
            return response
        else:
            return Error(form.errors)

    def about_get(self, user):
        return user.get_profile().about

    @form_handler
    def about_set(self, rdata, user):
        form = TutorForm(upost(rdata,user.get_profile()), instance=user.get_profile())
        if form.is_valid():
            form.save()
            response = RpcHttpResponse()
            response['success'] = True
            response['msg'] = "about saved"
            return response
        else:
            return Error(form.errors)
    
    # education begin
    
    def education_form_get(self, user):
        education = user.get_profile().education.all()
        rendered = render_to_string('tutors/blocks/education_form.html', {'education': education})
        return rendered

    def education_change_form_get(self, edu_id, user, request):
        try:
            edu = Education.objects.get(pk=edu_id)
        except Education.DoesNotExist:
            return Error(_("Update error. Entry not found"))
        else:
            if not edu.tutor.user == user:
                return Error(_("Update error. Access denied"))
        rendered = render_to_string('tutors/blocks/education_change_form.html', RequestContext(request, {
            'edu': edu,
            'period_start': edu.period_start.year if edu.period_start else None,
            'period_end': edu.period_end.year if edu.period_end else None,
            'years': range(1990,2014)
        }))
        return rendered

    education_change_form_get._extra_kwargs = extra_kwargs

    def education_add_form_get(self, user, request):
        rendered = render_to_string('tutors/blocks/education_change_form.html', RequestContext(request, {
            'years': range(1990,2014)
        }))
        return rendered

    education_add_form_get._extra_kwargs = extra_kwargs

    @get_json
    def education_get(self, user):
        return user.get_profile().education.all()

    @form_handler
    def education_set(self, rdata, user):
        rdata.update({ 'tutor':user.get_profile().pk })
        if rdata['period_start']:
            rdata['period_start'] = datetime.strptime(rdata['period_start'],"%Y")
        if rdata['period_end']:
            rdata['period_end'] = datetime.strptime(rdata['period_end'],"%Y")
        if 'instance_id' in rdata and rdata['instance_id']:
            edu_id = rdata.pop('instance_id')[0]
            try:
                edu = Education.objects.get(pk=edu_id)
            except Education.DoesNotExist:
                return Error(_("Update error. Entry not found"))
            else:
                if not edu.tutor.user == user:
                    return Error(_("Update error. Access denied"))
                if 'action_delete' in rdata and rdata['action_delete']:
                    edu.delete()
                    response = RpcHttpResponse()
                    response['success'] = True
                    response['msg'] = "deleted"
                    return response
                form = EducationForm(rdata, instance=edu)
        else:
            form = EducationForm(rdata)
        if form.is_valid():
            form.save()
            response = RpcHttpResponse()
            response['success'] = True
            response['msg'] = "saved"
            return response
        else:
            return Error(form.errors)
        
    # education end
        
    # experience begin

    def experience_form_get(self, user):
        experience = user.get_profile().experience.all()
        rendered = render_to_string('tutors/blocks/experience_form.html', {'experience': experience})
        return rendered

    def experience_change_form_get(self, exp_id, user, request):
        try:
            exp = Experience.objects.get(pk=exp_id)
        except Experience.DoesNotExist:
            return Error(_("Update error. Entry not found"))
        else:
            if not exp.tutor.user == user:
                return Error(_("Update error. Access denied"))
        rendered = render_to_string('tutors/blocks/experience_change_form.html', RequestContext(request, {
            'exp': exp,
            'period_start': exp.period_start.year if exp.period_start else None,
            'period_end': exp.period_end.year if exp.period_end else None,
            'years': range(1990,2014),
        }))
        return rendered

    experience_change_form_get._extra_kwargs = extra_kwargs

    def experience_add_form_get(self, user, request):
        rendered = render_to_string('tutors/blocks/experience_change_form.html', RequestContext(request, {
            'years': range(1990,2014)
        }))
        return rendered

    experience_add_form_get._extra_kwargs = extra_kwargs

    @get_json
    def experience_get(self, user):
        return user.get_profile().experience.all()

    @form_handler
    def experience_set(self, rdata, user):
        rdata.update({ 'tutor':user.get_profile().pk })
        if rdata['period_start']:
            rdata['period_start'] = datetime.strptime(rdata['period_start'],"%Y")
        if rdata['period_end']:
            rdata['period_end'] = datetime.strptime(rdata['period_end'],"%Y")
        if 'instance_id' in rdata and rdata['instance_id']:
            exp_id = rdata.pop('instance_id')[0]
            try:
                exp = Experience.objects.get(pk=exp_id)
            except Experience.DoesNotExist:
                return Error(_("Update error. Entry not found"))
            else:
                if not exp.tutor.user == user:
                    return Error(_("Update error. Access denied"))
                if 'action_delete' in rdata and rdata['action_delete']:
                    exp.delete()
                    response = RpcHttpResponse()
                    response['success'] = True
                    response['msg'] = "deleted"
                    return response
                form = ExperienceForm(rdata, instance=exp)
        else:
            form = ExperienceForm(rdata)
        if form.is_valid():
            form.save()
            response = RpcHttpResponse()
            response['success'] = True
            response['msg'] = "saved"
            return response
        else:
            return Error(form.errors)

    # experience end

    # slots begin

    @get_json
    def slots_get(self, start, end, user):
        return user.get_profile().slots.filter(start__gte=start, end__lte=end)

    @form_handler
    def slots_set(self, rdata, user):
        rdata.update({'tutor':user.get_profile().pk})
        if 'instance_id' in rdata and rdata['instance_id']:
            exp_id = rdata.pop('instance_id')[0]
            try:
                exp = Slot.objects.get(pk=exp_id)
            except Slot.DoesNotExist:
                return Error(_("Update error. Entry not found"))
            else:
                if not exp.tutor.user == user:
                    return Error(_("Update error. Access denied"))
                if 'action_delete' in rdata and rdata['action_delete']:
                    exp.delete()
                    response = RpcHttpResponse()
                    response['success'] = True
                    response['msg'] = "deleted"
                    return response
                form = SlotForm(rdata, instance=exp)
        else:
            form = SlotForm(rdata)
        if form.is_valid():
            form.save()
            response = RpcHttpResponse()
            response['success'] = True
            response['msg'] = "saved"
            return response
        else:
            return Error(form.errors)


    # slots end