# -*- encoding: utf-8 -*-
from lib.djangorpc import Error, Msg, RpcHttpResponse
from util import timezone
from django.conf import settings
from django.utils.translation import ugettext as _
import time

from app.zooz import ZoozServerAPI, ZoozException
from app.zooz.models import Transaction as ZoozTransaction
from app.bookings.models import Order, PAYMENT_METHOD_ZOOZ

def extra_kwargs(request, *args, **kwargs):
    return {
        'request': request,
    }

zooz = ZoozServerAPI(settings.ZOOZ_API_URL, settings.ZOOZ_WEB_URL, settings.ZOOZ_UNIQUE_ID,
                                 settings.ZOOZ_APP_KEY, settings.ZOOZ_API_USERNAME, settings.ZOOZ_API_PASSWORD)


class ZoozApiClass(object):

    def open_trans(self, order_id, user):
        response = RpcHttpResponse()

        try:
            order = Order.objects.get(id = order_id)
        except Order.DoesNotExist:
            response['success'] = False
            response['msg'] = _("Order not found")
            return response

        if not order.user == user:
            response['success'] = False
            response['msg'] = _("Access denied")

        try:
            token = zooz.open_trans(order.price, order.currency)
            response['token'] = token
        except ZoozException, e:
            response['success'] = False
            response['msg'] = e
            return response
        else:
            response['success'] = True

        transaction = ZoozTransaction(token=token, amount=order.price, currency=order.currency)
        transaction.save()
        order.transaction = transaction
        order.payment_method = PAYMENT_METHOD_ZOOZ
        order.save()

        return response
    def commit_trans(self, order_id, token, trx_id, display_id, status, user):
        response = RpcHttpResponse()

        try:
            order = Order.objects.get(id = order_id)
        except Order.DoesNotExist:
            response['success'] = False
            response['msg'] = _("Order not found")
            return response

        if not order.user == user:
            response['success'] = False
            response['msg'] = _("Access denied")
            return response

        try:
            transaction = ZoozTransaction.objects.get(token=token)
        except ZoozTransaction.DoesNotExist:
            response['success'] = False
            response['msg'] = _("Transation not valid")
            return response

        if not transaction == order.transaction:
            response['success'] = False
            response['msg'] = _("Wrong transaction commit for this order")
            return response

        transaction.trx_id = trx_id
        transaction.display_id = display_id
        transaction.status = status
        transaction.save()

        if not zooz.commit_trans(trx_id):
            response['success'] = False
            response['msg'] = _("Zooz payment failed")
            return response

        try:
            res = zooz.get_transaction_details(trx_id)
        except ZoozException, e:
            response['success'] = False
            response['msg'] = e
            return response

        if res['transactionStatus'] == 'Succeed' and res['actualPaid'] == order.price:
            transaction.completed_at = timezone.now()
            transaction.is_complete = True
            order.completed_at = transaction.completed_at
            order.is_complete = True
            order.save()
            response['success'] = True
        else:
            response['success'] = False
            response['msg'] = _("Zooz payment is not correct")

        transaction.status = res['transactionStatus']
        transaction.save()

        return response
