# -*- encoding: utf-8 -*-
__author__ = 'shamanu4.at.gmail.dot.com'

from django.test import TestCase
from django.conf import settings
from app.zooz import ZoozServerAPI, QUERY_TYPE_API, QUERY_TYPE_WEB, ZoozException


class ZoozAPITestCase(TestCase):
    trans_id = None

    @classmethod
    def setUpClass(cls):
        cls.zooz = ZoozServerAPI(settings.ZOOZ_API_URL, settings.ZOOZ_WEB_URL, settings.ZOOZ_UNIQUE_ID,
                                 settings.ZOOZ_APP_KEY, settings.ZOOZ_API_USERNAME, settings.ZOOZ_API_PASSWORD)

    def test_01_zooz_query_open_trans(self):
        res = self.zooz.query({
            'cmd': 'openTrx',
            'amount': '0.99',
            'currencyCode': 'USD'}, QUERY_TYPE_WEB)
        self.assertEqual(res['statusCode'], ['0'])
        self.trans_id = res['token']

    def test_02_zooz_query_commit_trans(self):
        with self.assertRaisesMessage(ZoozException, "Payment token is corrupted."):
            self.zooz.query({
                'cmd': 'commitTransaction',
                'transactionID': self.trans_id
            }, QUERY_TYPE_API)

    def test_03_zooz_open_trans(self):
        token = self.zooz.open_trans(0.99, 'USD')
        self.assertIsInstance(token, str)
