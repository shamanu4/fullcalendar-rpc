# -*- encoding: utf-8 -*-
import urllib2, urllib
try:
   from urlparse import parse_qs
except ImportError:
   from cgi import parse_qs

def yourView(request):
  isSandbox = True
  if isSandbox == True:
    url = 'https://sandbox.zooz.co/mobile/SecuredWebServlet'
  else:
    url = 'https://app.zooz.com/mobile/SecuredWebServlet'

  req = urllib2.Request(url)
  req.add_header('ZooZUniqueID', 'com.zooz.mobileweb.sample')
  req.add_header('ZooZAppKey', 'my-app-key')
  req.add_header('ZooZResponseType', 'NVP')

  if 'cmd' in request.GET and request.GET['cmd'] == 'openTrx':
    postData = {'cmd' : 'openTrx',
              'amount' : '0.99',
              'currencyCode' : 'USD' }
    postDataEncoded = urllib.urlencode(postData)
    req.add_data(postDataEncoded)
    res = urllib2.urlopen(req)
    result = res.read()
    results = parse_qs(result)
    if 'sessionToken' in results:
      token = results['sessionToken'][0]
      token = token.rstrip()
      return HttpResponse( "var data = {'token' : '" + token + "'}")
    else:
      return HttpResponse(result)
  else:
    if 'cmd' in request.GET and request.GET['cmd'] == 'openTrx':
      trxId = request.GET['trxId']
    else:
      return HttpResponse('trxId is missing')
    postData = {'cmd' : 'verifyTrx',
                'transactionID' : trxId}
    postDataEncoded = urllib.urlencode(postData)
    req.add_data(postDataEncoded)
    res = urllib2.urlopen(req)
    result = res.read()
    results = urlparse.parse_qs(result)
    return HttpResponse(result)