# -*- encoding: utf-8 -*-
__author__ = 'maxim'

from django.db import models
from django.utils.translation import ugettext as _
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes import generic

from util import timezone

class Transaction(models.Model):
    created_at = models.DateTimeField(default=timezone.now)
    completed_at = models.DateTimeField(blank=True, null=True)
    is_complete = models.BooleanField(default=False)
    amount = models.DecimalField(max_digits=5, decimal_places=2, default=0)
    currency = models.CharField(max_length=4, blank=True, null=True)
    token = models.CharField(max_length=200, blank=True, null=True)
    trx_id = models.CharField(max_length=40, blank=True, null=True)
    display_id = models.PositiveIntegerField(blank=True, null=True)
    status = models.CharField(max_length=40, blank=True, null=True)
    orders = generic.GenericRelation("bookings.Order")

    def __unicode__(self):
        return "%s [%s %s]" % (self.created_at, self.amount, self.currency)