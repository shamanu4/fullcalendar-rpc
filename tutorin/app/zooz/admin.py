# -*- encoding: utf-8 -*-
from django.contrib import admin
from .models import Transaction


class TransactionAdmin(admin.ModelAdmin):
    list_display = ('created_at', 'is_complete', 'completed_at', 'amount', 'currency', 'status')
    list_filter = ('is_complete', 'status')
admin.site.register(Transaction, TransactionAdmin)


