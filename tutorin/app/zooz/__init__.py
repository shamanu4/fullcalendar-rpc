# -*- encoding: utf-8 -*-
__author__ = 'maxim'

import urllib2, urllib

try:
    from urlparse import parse_qs
except ImportError:
    from cgi import parse_qs

import json


QUERY_TYPE_WEB = 0
QUERY_TYPE_API = 1


class ZoozException(Exception):
    pass


class ZoozServerAPI(object):
    def __init__(self, api_url, web_url, app_id, app_key, api_username, api_password):
        self.api_url = api_url
        self.web_url = web_url
        self.app_id = str(app_id)
        self.app_key = str(app_key)
        self.api_username = str(api_username)
        self.api_password = str(api_password)
        self.response_type = 'JSon'

    def query(self, data, query_type):
        postDataEncoded = urllib.urlencode(data)
        if query_type == QUERY_TYPE_WEB:
            req = urllib2.Request(self.web_url)
        elif query_type == QUERY_TYPE_API:
            req = urllib2.Request(self.api_url)
        else:
            raise ZoozException('Zooz query type is invalid')
        req.add_header('ZooZUniqueID', self.app_id)
        req.add_header('ZooZAppKey', self.app_key)
        req.add_header('ZooZResponseType', self.response_type)
        req.add_header('ZooZDeveloperId', self.api_username)
        req.add_header('ZooZServerPassword', self.api_password)
        req.add_data(postDataEncoded)
        res = urllib2.urlopen(req)
        result = res.read()
        try:
            results = json.loads(result)
        except ValueError:
            results = parse_qs(result)
        if 'ResponseStatus' in results and not results['ResponseStatus'] == 0:
            raise ZoozException(results['ResponseObject']['errorMessage'])
        return results

    def open_trans(self, amount, currency):
        res = self.query({
                'cmd': 'openTrx',
                'amount': str(amount),
                'currencyCode': str(currency)
            }, QUERY_TYPE_WEB)
        try:
            assert res['statusCode'] == ['0']
        except (ZoozException, KeyError, AssertionError), e:
            raise ZoozException(e)
        try:
            return res['token'][0]
        except (KeyError, IndexError), e:
            raise ZoozException(e)

    def commit_trans(self, trx_id):
        res = self.query({
                'cmd': 'commitTransaction',
                'transactionID': trx_id
            }, QUERY_TYPE_API)
        if 'ResponseStatus' in res and res['ResponseStatus'] == 0:
            if 'ResponseObject' in res:
                if 'boolean' in res['ResponseObject']:
                    return res['ResponseObject']['boolean']
        return False

    def get_transaction_details(self, trx_id):
        res = self.query({
                'cmd': 'getTransactionDetails',
                'transactionID': trx_id
            }, QUERY_TYPE_API)
        if 'ResponseStatus' in res and res['ResponseStatus'] == 0:
            if 'ResponseObject' in res:
                return  res['ResponseObject']
