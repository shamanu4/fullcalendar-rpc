# -*- encoding: utf-8 -*-
from lib.djangorpc import RpcRouter, Error, Msg, RpcHttpResponse
from app.tutors.actions import TutorApiClass
from app.bookings.actions import BookingApiClass
from app.zooz.actions import ZoozApiClass

router = RpcRouter('router', {
    'TutorApi': TutorApiClass(),
    'BookingApi': BookingApiClass(),
    'ZoozApi': ZoozApiClass(),
})
